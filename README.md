### Proyecto sophos 
Este proyecto tiene la finalidad de poder evaluar los conocimiento técnicos de Hernan Jose Malave Montero en el área de Test Automation

<h5>Detalles</h5>
<br>Este proyecto está elaborado de la siguiente manera:
Lenguaje de programación: java
Framework: Gradle
Pátron de diseño: Screenplay
Reporteador: Serenity


<h5>Estructura</h5>
<ul>

<li> Carpeta Task: Encontrará las tareas con las que debe ejecutar el actor para la automatización </li>

<li> Carpeta Interaction: Encontrará las interacciones que debe hacer el usuario con el front para ejecutar las tareas, se observa una clase llamada Click y Type, que a pesar de que serennity tiene esas funciones en sus librerias se adicionan al proyectos para poder configurar los tiempos implicitos</li>

<li> Carpeta Models: Encontrará la data con que ejecutamos la automatización, la compartimos entre las otras clases utilizando el @Shared.</li>

<li> Carpeta Utils: Encontrará las clases que apoyan al actor para ejecutar acciones especificas, generar números aleatorios, obtener palabras reservadas </li>

<li> UserInterface: Encontrará refernciados los diferentes elementos de nuestro portal para ser reutilizados las veces que seán necesarias.</li>

</ul>





