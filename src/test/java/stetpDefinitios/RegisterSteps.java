package stetpDefinitios;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.DataRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Shared;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.openqa.selenium.WebDriver;
import task.Login;
import task.Register;
import util.Words;

public class RegisterSteps {

    @Managed (driver = "Chrome")
    WebDriver herdriver;
    Actor user = Actor.named("user");

    @Shared
    DataRegister dataRegister;

    @Given("^user open the portal web$")
    public void user_open_the_portal_web() throws Throwable {
        user.can(BrowseTheWeb.with(herdriver));
        user.attemptsTo(Open.url(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL_HOME_PAGE)));
    }

    @When("^user enter your date$")
    public void user_enter_your_date() {
        user.attemptsTo(Register.user());
    }

    @Then("^user could do login$")
    public void user_could_do_login() {
        user.attemptsTo(Login.user(dataRegister.getEMAIL(), dataRegister.getPASSWORD()));
    }

}
