package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/Feature/",
        glue = "stetpDefinitios",
        snippets = SnippetType.CAMELCASE,
        dryRun = false,
        tags = "@RegisterSuccessful  "
)



public class testRunner {
}
