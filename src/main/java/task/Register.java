package task;

import interactions.Click;
import interactions.Loop;
import interactions.Type;
import interactions.WaitElement;
import models.DataRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.thucydides.core.annotations.Shared;
import userInterface.HomePage;
import userInterface.RegisterPage;
import util.NumberRamdom;

public class Register implements Task {

    @Shared
    DataRegister dataRegister;


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(HomePage.BUTTON_JOIN_TODAY));
        actor.attemptsTo(Type.on(RegisterPage.INPUT_FIRST_NAME, dataRegister.getNAME()));
        actor.attemptsTo(Type.on(RegisterPage.INPUT_LAST_NAME, dataRegister.getLAST_NAME()));
        dataRegister.setEMAIL(dataRegister.getEMAIL_REFERENCE() + NumberRamdom.user() + dataRegister.getFORMAT_EMAIL());
        actor.attemptsTo(Type.on(RegisterPage.INPUT_EMAIL, dataRegister.getEMAIL()));
        actor.attemptsTo(SelectFromOptions.byVisibleText(dataRegister.getMES()).from(RegisterPage.LIST_BIRTH_MOUNTH));
        actor.attemptsTo(SelectFromOptions.byValue(dataRegister.getDIA()).from(RegisterPage.LIST_BIRTH_DAY));
        actor.attemptsTo(SelectFromOptions.byValue(dataRegister.getYEAR()).from(RegisterPage.LIST_BIRTH_YEAR));
        actor.attemptsTo(Scroll.to(RegisterPage.LIST_NEXT_LOCATION));
        actor.attemptsTo(Click.on(RegisterPage.LIST_NEXT_LOCATION));
        actor.attemptsTo(WaitElement.visible(RegisterPage.REFERENCE_ONE));
        actor.attemptsTo(Scroll.to(RegisterPage.LIST_NEXT_DEVICES));
        actor.attemptsTo(Click.on(RegisterPage.LIST_NEXT_DEVICES));
        actor.attemptsTo(WaitElement.visible(RegisterPage.REFERENCE_ONE));
        actor.attemptsTo(Click.on(RegisterPage.LIST_MOVILE_DEVICES));
        actor.attemptsTo(Loop.list(RegisterPage.LIST_DEVICE, dataRegister.getDEVICE()));
        actor.attemptsTo(Click.on(RegisterPage.LIST_MODEL_DEVICES));
        actor.attemptsTo(Loop.list(RegisterPage.LIST_MODEL, dataRegister.getMODEL_DEVICE()));
        actor.attemptsTo(Click.on(RegisterPage.LIST_SYSTEM_OPERATIVE));
        actor.attemptsTo(Loop.list(RegisterPage.LIST_DEVICE, dataRegister.getSYSTEM_OPERATIVE()));
        actor.attemptsTo(Click.on(RegisterPage.LIST_NEXT_DEVICES));
        actor.attemptsTo(Type.on(RegisterPage.INPUT_PASSWORD, dataRegister.getPASSWORD()));
        actor.attemptsTo(Type.on(RegisterPage.INPUT_CONFIRM_PASSWORD, dataRegister.getPASSWORD()));
        actor.attemptsTo(Scroll.to(RegisterPage.CHECK_BOX_FIRST));
        actor.attemptsTo(Click.on(RegisterPage.CHECK_BOX_FIRST));
        actor.attemptsTo(Scroll.to(RegisterPage.CHECK_BOX_SECONDS));
        actor.attemptsTo(Click.on(RegisterPage.CHECK_BOX_SECONDS));
        actor.attemptsTo(Scroll.to(RegisterPage.CHECK_BOX_THIRD));
        actor.attemptsTo(Click.on(RegisterPage.CHECK_BOX_THIRD));
        actor.attemptsTo(Click.on(RegisterPage.BUTTON_COMPLETE_SETUP));

    }
    public static Register user(){
        return Tasks.instrumented(Register.class);
    }
}
