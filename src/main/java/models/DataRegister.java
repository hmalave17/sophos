package models;

public class DataRegister {

    private String NAME;
    private String LAST_NAME;
    private String EMAIL_REFERENCE;
    private String EMAIL;
    private String FORMAT_EMAIL;
    private String MES;
    private String DIA;
    private String YEAR;
    private String DEVICE;
    private String MODEL_DEVICE;
    private String SYSTEM_OPERATIVE;
    private String PASSWORD;


    public DataRegister(){
        this.NAME = "Hernan";
        this.LAST_NAME = "Malave";
        this.EMAIL_REFERENCE = "Hernanmalave";
        this.FORMAT_EMAIL = "@test.com";
        this.MES = "April";
        this.DIA = "number:1";
        this.YEAR = "number:2000";
        this.DEVICE = "Apple";
        this.MODEL_DEVICE = "iPhone 4";
        this.SYSTEM_OPERATIVE = "iOS 11.2.1";
        this.PASSWORD = "Hmalave123456789*";
    }

    public String getNAME() {
        return this.NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getLAST_NAME() {
        return this.LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public String getEMAIL_REFERENCE() {
        return this.EMAIL_REFERENCE;
    }

    public void setEMAIL_REFERENCE(String EMAIL_REFERENCE) {
        this.EMAIL_REFERENCE = EMAIL_REFERENCE;
    }

    public String getEMAIL() {
        return this.EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getFORMAT_EMAIL() {
        return this.FORMAT_EMAIL;
    }

    public void setFORMAT_EMAIL(String FORMAT_EMAIL) {
        this.FORMAT_EMAIL = FORMAT_EMAIL;
    }

    public String getMES() {
        return this.MES;
    }

    public void setMES(String MES) {
        this.MES = MES;
    }

    public String getDIA() {
        return this.DIA;
    }

    public void setDIA(String DIA) {
        this.DIA = DIA;
    }

    public String getYEAR() {
        return this.YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getDEVICE() {
        return this.DEVICE;
    }

    public void setDEVICE(String DEVICE) {
        this.DEVICE = DEVICE;
    }

    public String getMODEL_DEVICE() {
        return this.MODEL_DEVICE;
    }

    public void setMODEL_DEVICE(String MODEL_DEVICE) {
        this.MODEL_DEVICE = MODEL_DEVICE;
    }

    public String getSYSTEM_OPERATIVE() {
        return this.SYSTEM_OPERATIVE;
    }

    public void setSYSTEM_OPERATIVE(String SYSTEM_OPERATIVE) {
        this.SYSTEM_OPERATIVE = SYSTEM_OPERATIVE;
    }

    public String getPASSWORD() {
        return this.PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }
}
