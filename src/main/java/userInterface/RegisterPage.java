package userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegisterPage extends PageObject {

    public static final Target INPUT_FIRST_NAME = Target.the("Input first name").located(By.id("firstName"));
    public static final Target INPUT_LAST_NAME = Target.the("Input last name").located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("Input email").located(By.id("email"));
    public static final Target LIST_BIRTH_MOUNTH = Target.the("List Birth Mounth").located(By.id("birthMonth"));
    public static final Target LIST_BIRTH_DAY = Target.the("List Birth day").located(By.id("birthDay"));
    public static final Target LIST_BIRTH_YEAR = Target.the("List Birth Year").located(By.id("birthYear"));
    public static final Target LIST_NEXT_LOCATION = Target.the("List next Location").located(By.className("btn-blue"));
    public static final Target REFERENCE_ONE = Target.the("Refernce one").located(By.className("step-intro"));
    public static final Target LIST_NEXT_DEVICES = Target.the("List next devices").located(By.className("btn-blue"));
    public static final Target LIST_MOVILE_DEVICES = Target.the("List movile device").located(By.name("handsetMakerId"));
    public static final Target LIST_DEVICE = Target.the("List device").located(By.className("ui-select-choices-row-inner"));
    public static final Target LIST_MODEL_DEVICES = Target.the("List model device").located(By.name("handsetModelId"));
    public static final Target LIST_MODEL = Target.the("List Models").located(By.className("ui-select-choices-row"));
    public static final Target LIST_SYSTEM_OPERATIVE = Target.the("List sistem operative").located(By.name("handsetOSId"));
    public static final Target INPUT_PASSWORD = Target.the("Input password").located(By.id("password"));
    public static final Target INPUT_CONFIRM_PASSWORD = Target.the("Input confirm password").located(By.id("confirmPassword"));
    public static final Target CHECK_BOX_FIRST = Target.the("Check box first").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[4]/label/span"));
    public static final Target CHECK_BOX_SECONDS = Target.the("check box seconds").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));
    public static final Target CHECK_BOX_THIRD = Target.the("check box third").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));
    public static final Target BUTTON_COMPLETE_SETUP = Target.the("Button complete setup").located(By.id("laddaBtn"));

}
