package userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomePage extends PageObject {

    public static final Target BUTTON_JOIN_TODAY = Target.the("Button join today").located(By.className("unauthenticated-nav-bar__sign-up"));
    public static final Target BUTTON_LOGIN = Target.the("Button Login").located(By.xpath("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[1]/a"));
}
